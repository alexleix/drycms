### 项目介绍

DRYCMS是一款基于PHP7(swoole)+MYSQL的智能内容管理系统，后台几乎不需要写代码，支持docker部署。

您可以照着文档里面的`实践->多级分类`操作一遍，然后就知道后台不需要写代码是怎么实现的了。

您也可以观看我们录制的视频，更快地了解DRYCMS是不是您想要的软件。

我们用go语言专门为用户写了一个CDN程序，可以将项目相关的软件快速下载到本地，让部署非常简单。

### 开发计划

1.免费版的后台模板-已完成

2.基于DRYCMS的博客系统

### 后台效果图

[去看看](https://www.drycms.com)

### 文档地址

[文档](https://www.drycms.com)

### 开源协议

MIT

### 演示地址

http://116.62.156.219

用户名 administrator

密码 drycms@3721

每天24点重置一次数据库

### 联系我们

QQ群 1109033476

知乎 DRYCMS

### 视频教程

视频列表

https://space.bilibili.com/520869934

建议PC网页1080P观看

`1-DRYCMS的安装`

https://www.bilibili.com/video/BV1gK4y1t7pA

`2-ubuntu18.04安装docker并运行mysql和redis`

https://www.bilibili.com/video/BV1Le411W7bh

`3-DRYCMS功能演示`

https://www.bilibili.com/video/BV18i4y147Ei

`4-DRYCMS后台不写代码做一个增删改查的功能`

https://www.bilibili.com/video/BV1tZ4y1W7gX

`5-DRYCMS后台强大的搜索配置功能`

https://www.bilibili.com/video/BV1Dk4y1z7PF

`6-DRYCMS后台无与伦比的文件管理功能`

https://www.bilibili.com/video/BV1ip4y1D7ig

`7-DRYCMS针对后台免费模版的安装及演示`

https://www.bilibili.com/video/BV17D4y1Q74H

### 子妹篇

1.go语言开发的web框架，助你开发稳准快。

[gin_new](https://gitee.com/xxfaxy/gin_new)

2.go语言开发的微服务框架，待开源。

3.金黔猫小程序源码，待开源。