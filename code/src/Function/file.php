<?php
function readFileFromDir($dir, $list = [])
{
    $handle = opendir($dir);
    while(($file = readdir($handle)) !== false){
        if(in_array($file, ['.', '..'])){
            continue;
        }
        $file = $dir . DIRECTORY_SEPARATOR . $file;
        if(is_file($file)){
            $list[] = $file;
        }
        else if(is_dir($file)){
            $list = readFileFromDir($file, $list);
        }
    }
    closedir($handle);
    return $list;
}

function isPhpFile($file)
{
    $pi = pathinfo($file);
    return file_exists($file) && $pi['extension'] == 'php';
}