<?php
namespace Pay;
use Yansongda\Pay\Pay;

class WeiXinSp extends Base
{

    private function getPayConfig()
    {
        $temp = getConfig('weixin', 'sp');
        $config = [
            'miniapp_id' => $temp['app_id'],
            'mch_id' => $temp['mch_id'],
            'key' => $temp['app_secret'],
            'notify_url' => $temp['notify_url']
        ];
        return $config;
    }

    /*
        支付信息
    */
    public function getPay($openId, $name, $orderNo, $amount)
    {
        $order = [
            'body' => $name,
            'out_trade_no' => $orderNo,
            'total_fee' => money2weixin($amount),
            'openid' => $openId
        ];
        return Pay::wechat($this->getPayConfig())->miniapp($order);
    }

    /*
        查询支付信息
    */
    public function queryPay($orderNo)
    {
        $config = $this->getPayConfig();
        $config['app_id'] = $config['miniapp_id'];
        unset($config['miniapp_id']);
        $order = [
            'out_trade_no' => $orderNo
        ];
        return Pay::wechat($config)->find($order);
    }

}