<?php
namespace Cache;

class UserToken extends Base
{

    private $prefix = 'user_token';

    public function __construct()
    {
        $this->redis = $this->get(CORE_REDIS)->get('user_token');
    }

    public function getTokenKey($device, $id)
    {
        return sprintf('%s_%s_%s', $this->prefix, $device, $id);
    }

    public function setToken($device, $id, $token)
    {
        $key = $this->getTokenKey($device, $id);
        return $this->redis->setEx($key, TOKEN_TIME, $token);
    }

    public function getToken($device, $id)
    {
        $key = $this->getTokenKey($device, $id);
        return $this->redis->get($key);
    }

    public function deleteToken($device, $id)
    {
        $key = $this->getTokenKey($device, $id);
        return $this->redis->del($key);
    }

}