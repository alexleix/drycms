<?php
namespace Cache;

class Common extends Base
{

    private $prefix = 'common';

    public function __construct()
    {
        $this->redis = $this->get(CORE_REDIS)->get('common');
    }

    public function getKey($name)
    {
        return sprintf('%s_%s', $this->prefix, $name);
    }

}