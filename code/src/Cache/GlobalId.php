<?php
namespace Cache;

class GlobalId extends Base
{

    private $prefix = 'global';

    public function __construct()
    {
        $this->redis = $this->get(CORE_REDIS)->get('global');
    }

    public function getKey($table)
    {
        return sprintf('%s_%s', $this->prefix, $table);
    }

    public function getId($table)
    {
        $key = $this->getKey($table);
        return $this->redis->incr($key);
    }

}