<?php
namespace Cache;

class ApiLimit extends Base
{

    public function __construct()
    {
        $this->redis = $this->get(CORE_REDIS)->get('api_limit');
    }

}