<?php
namespace CommonTrait;

trait Base
{

    public function get($name)
    {
        return getContainer()->get($name);
    }

    public function getInstance($name)
    {
        return getContainer()->getInstance($name);
    }

}