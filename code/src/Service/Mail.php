<?php
namespace Service;

class Mail extends Base
{

    public function send($subject, $body, $to = [])
    {
        $config = getConfig('mail', THE_DEFAULT);
        $mail = new \PHPMailer(true);
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host = $config['host'];
        $mail->SMTPAuth = true;
        $mail->Username = $config['username'];
        $mail->Password = $config['password'];
        $mail->SMTPSecure = $config['secure'];
        $mail->Port = $config['port'];
        $mail->setFrom($config['username']);
        foreach($to as $v){
            $mail->addAddress($v);
        }
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->send();
    }

}