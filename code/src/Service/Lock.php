<?php
namespace Service;

class Lock extends Base
{

    private $cacheLock = null;

    public function __construct()
    {
        $this->cacheLock = $this->get(CACHE_LOCK);
    }

    public function hasLock($name, $time)
    {
        return $this->cacheLock->hasLock($name, $time);
    }

    public function removeLock($name)
    {
        return $this->cacheLock->removeLock($name);
    }

    public function isFirst($name, $time)
    {
        if($this->hasLock($name, $time)){
            return false;
        }
        return true;
    }

}