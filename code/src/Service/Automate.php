<?php
namespace Service;

class Automate extends Base
{

    private $config = '';

    private $table = '';

    public function setConfigTable($config, $table)
    {
        $this->config = $config;
        $this->table = $table;
        return $this;
    }

    public function setReference($model)
    {
        return $this->setConfigTable($model->dry_database_config_key, $model->dry_table);
    }

    public function setAlias($alias = '')
    {
        return $this->setReference($this->get(SERVICE_MODEL)->getModelByAlias($alias));
    }

    public function getTable()
    {
        return $this->table;
    }

    public function getDatabase()
    {
        $model = $this->get(MODEL_AUTOMATE);
        $model->config = $this->config;
        $model->table = $this->table;
        $database = $this->get(DATABASE_AUTOMATE);
        $database->resetModel($model);
        return $database;
    }

    public function getListForChoice($keyField = '', $valueField = '', $sql = '', $extension = [])
    {
        $database = $this->getDatabase();
        return $database->getListForChoice($keyField, $valueField, $sql, $extension);
    }

    public function getLanguageForMakeFile()
    {
        $database = $this->getDatabase();
        return $database->getLanguageForMakeFile();
    }

    public function getChoiceForMakeFile()
    {
        $database = $this->getDatabase();
        return $database->getChoiceForMakeFile();
    }

    public function getConstantForMakeFile()
    {
        $database = $this->getDatabase();
        return $database->getConstantForMakeFile();
    }

    public function getCategoryList()
    {
        $database = $this->getDatabase();
        return $database->getCategoryList();
    }

    public function makeVariableOrUrl($field)
    {
        if($field->dry_domain == '*'){
            $field->dry_domain = getDomain();
        }
        $source = $field->dry_data_source;
        $result = '';
        if($source == 1){
            $result = "variable='{$field->dry_variable}'";
        }
        else if($source == 2){
            $result = "url='{$field->dry_domain}{$field->dry_api}'";
        }
        else if($source == 3){
            $result = "url='{$field->dry_domain}{$field->dry_api}?alias={$field->dry_model_alias}&fieldId={$field->id}'";
        }
        else if($source == 4){
            $result = "url='/Api/DataPool/getData?name={$field->dry_data_pool_name}'";
        }
        return $result;
    }

    public function makeUrl($field)
    {
        if($field->dry_domain == '*'){
            $field->dry_domain = getDomain();
        }
        $source = $field->dry_data_source;
        if($source == 2){
            return $field->dry_domain . $field->dry_api;
        }
        else if($source == 3){
            return "{$field->dry_domain}{$field->dry_api}?alias={$field->dry_model_alias}&fieldId={$field->id}";
        }
        else if($source == 4){
            return "/Api/DataPool/getData?name={$field->dry_data_pool_name}";
        }
        return '';
    }

}