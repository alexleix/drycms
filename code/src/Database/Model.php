<?php
namespace Database;

class Model extends Base
{

    public function __construct()
    {
        $this->setModel(MODEL_MODEL);
    }

    public function getIndex($page, $limit, $extension = [])
    {
        return $this->getIndexByOrder($page, $limit, 'dry_sort', 'asc');
    }

    public function getModelByAlias($alias = '')
    {
        $sql = $this->getSql();
        $sql->field('*');
        $sql->table($this->getTable());
        $sql->where('dry_alias', '=', $alias);
        $model = $this->fetch($sql->get());
        return $model;
    }

    public function getAliasControllerName()
    {
        $sql = $this->getSql();
        $sql->field('dry_alias,dry_controller_name');
        $sql->table($this->getTable());
        $list = $this->fetchAll($sql->get());
        return $list;
    }

}