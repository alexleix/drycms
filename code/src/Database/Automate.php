<?php
namespace Database;

class Automate extends Base
{

    public function __construct()
    {
        $this->setModel(MODEL_AUTOMATE);
    }

    public function resetModel($model = null)
    {
        $this->setModel($model);
    }

    public function getIndex($page, $limit, $extension = [])
    {
        $sql = $this->getSql();
        $sql->field('*');
        $sql->table($this->getTable());
        $sql->setPage($page);
        $sql->setPageSize($limit);
        $sql->setLimit();
        if(isset($extension['sort']) && !empty($extension['sort'])){
            $list = explode(',', $extension['sort']);
            foreach($list as $item){
                list($field, $type) = explode(' ', $item);
                $sql->setOrder($field, $type);
            }
        }
        else{
            $sql->setOrder('id', 'desc');
        }
        /*搜索 start*/
        if(!empty($extension['search'])){
            $sql->whereRaw('1=1');
        }
        foreach($extension['search'] as $field => $value){
            /*字段是加密字段*/
            $encrypt = false;
            if(startWith($field, '*')){
                $encrypt = true;
                $field = substr($field, 1);
            }
            $value = explode('|*|', $value);
            $type = $value[0];
            if($type == 'select'){
                if($value[1] != 'all'){
                    $sql->andWhere($field, 'contain', $value[1]);
                }
            }
            else if($type == 'input'){
                $operate = $value[1];
                $value0 = $value[2];
                $value1 = $value[3];
                /*只需要加密第一个值*/
                if($encrypt && !empty($value0)){
                    $value0 = $this->get(CORE_ENCRYPT)->setKey('sensitive')->encrypt($value0);
                }
                $result = $this->search($sql, $field, $operate, $value0, $value1);
                if($result){
                    $sql = $result;
                }
            }
        }
        /*搜索 end*/
        $list = $this->fetchAll($sql->get());
        $count = $this->fetchColumn($sql->getTotalSql());
        return [
            'list' => $list,
            'count' => $count
        ];
    }

    private function search($sql, $field, $operate, $value0, $value1)
    {
        $require0 = ['is_null', 'is_not_null', 'is_empty', 'is_not_empty'];
        $require2 = ['between_and', 'not_between_and'];
        if(in_array($operate, $require2) && (empty($value0) || empty($value1))){
            return false;
        }
        if(!in_array($operate, $require0) && !in_array($operate, $require2) && empty($value0)){
            return false;
        }
        switch($operate){
            case 'lt':
                $sql->andWhere($field, '<', $value0);
                break;
            case 'lte':
                $sql->andWhere($field, '<=', $value0);
                break;
            case 'equal':
                $sql->andWhere($field, '=', $value0);
                break;
            case 'gte':
                $sql->andWhere($field, '>=', $value0);
                break;
            case 'gt':
                $sql->andWhere($field, '>', $value0);
                break;
            case 'not_equal':
                $sql->andWhere($field, '!=', $value0);
                break;
            case 'left_like_right':
                $sql->andWhere($field, 'like', "%{$value0}%");
                break;
            case 'left_not_like_right':
                $sql->andWhere($field, 'not like', "%{$value0}%");
                break;
            case 'like_right':
                $sql->andWhere($field, 'like', "{$value0}%");
                break;
            case 'not_like_right':
                $sql->andWhere($field, 'not like', "{$value0}%");
                break;
            case 'left_like':
                $sql->andWhere($field, 'like', "%{$value0}");
                break;
            case 'left_not_like':
                $sql->andWhere($field, 'not like', "%{$value0}");
                break;
            case 'is_null':
                $sql->whereRaw(" and {$field} is null");
                break;
            case 'is_not_null':
                $sql->whereRaw(" and {$field} is not null");
                break;
            case 'is_empty':
                $sql->whereRaw(" and {$field} = ''");
                break;
            case 'is_not_empty':
                $sql->whereRaw(" and {$field} != ''");
                break;
            case 'between_and':
                $sql->andWhere($field, 'between and', [$value0, $value1]);
                break;
            case 'not_between_and':
                $sql->andWhere($field, 'not between and', [$value0, $value1]);
                break;
            case 'in':
                $sql->andWhere($field, 'in', $value0);
                break;
            case 'not_in':
                $sql->andWhere($field, 'not in', $value0);
                break;
            case 'contain':
                $sql->andWhere($field, 'contain', $value0);
                break;
            case 'not_contain':
                $sql->andWhere($field, 'not contain', $value0);
                break;
            case 'match_word':
                $sql->andWhere($field, 'match word', $value0);
                break;
        }
        return $sql;
    }

    public function getListForChoice($keyField = '', $valueField = '', $sql = '', $extension = [])
    {
        /*
            优先使用自定义sql
        */
        if($sql != ''){
            $sql = str_replace('@table', $this->getTable(), $sql);
            return $this->fetchAll($sql);
        }
        $sql = $this->getSql();
        $sql->field("*,{$keyField} as k,{$valueField} as v");
        $sql->table($this->getTable());
        return $this->fetchAll($sql->get());
    }

    public function getLanguageForMakeFile()
    {
        $sql = $this->getSql();
        $sql->field('*');
        $sql->table($this->getTable());
        $sql->setOrder('dry_group', 'asc');
        $sql->setOrder('dry_key', 'asc');
        return $this->fetchAll($sql->get());
    }

    public function getChoiceForMakeFile()
    {
        $sql = $this->getSql();
        $sql->field('*');
        $sql->table($this->getTable());
        $sql->setOrder('dry_data_sort', 'asc');
        return $this->fetchAll($sql->get());
    }

    public function getConstantForMakeFile()
    {
        $sql = $this->getSql();
        $sql->field('*');
        $sql->table($this->getTable());
        $sql->setOrder('dry_group', 'asc');
        $sql->setOrder('dry_sort', 'asc');
        return $this->fetchAll($sql->get());
    }

    public function getCategoryList()
    {
        $sql = $this->getSql();
        $sql->field('*');
        $sql->table($this->getTable());
        $sql->where('dry_status', '=', 1);
        $sql->setOrder('dry_data_sort', 'asc');
        return $this->fetchAll($sql->get());
    }

}