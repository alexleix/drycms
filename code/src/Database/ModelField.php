<?php
namespace Database;

class ModelField extends Base
{

    public function __construct()
    {
        $this->setModel(MODEL_MODELFIELD);
    }

    public function getIndex($page, $limit, $extension = [])
    {
        $sql = $this->getSql();
        $sql->field('*');
        $sql->table($this->getTable());
        $sql->setPage($page);
        $sql->setPageSize($limit);
        $sql->setLimit();
        $sql->setOrder('dry_model', 'asc');
        $sql->setOrder('dry_sort', 'asc');
        if(isset($extension['modelId']) && $extension['modelId'] > 0){
            $sql->where('dry_model', '=', $extension['modelId']);
        }
        $list = $this->fetchAll($sql->get());
        $count = $this->fetchColumn($sql->getTotalSql());
        return [
            'list' => $list,
            'count' => $count
        ];
    }

    public function deleteByModelId($modelId)
    {
        $sql = $this->getSql();
        $sql->table($this->getTable());
        $sql->where('dry_model', '=', $modelId);
        return $this->runDelete($sql->getDeleteSql());
    }

    /*
        编辑时删除不需要的字段
    */
    public function deleteByExclude($modelId, $fieldIn)
    {
        $sql = $this->getSql();
        $sql->table($this->getTable());
        $sql->where('dry_model', '=', $modelId);
        $sql->andWhere('dry_field', 'not in', $fieldIn);
        return $this->runDelete($sql->getDeleteSql());
    }

    public function getListByModelId($modelId)
    {
        $sql = $this->getSql();
        $sql->table($this->getTable());
        $sql->where('dry_model', '=', $modelId);
        $sql->setOrder('dry_sort', 'asc');
        return $this->fetchAll($sql->get());
    }

}