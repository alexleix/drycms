<?php
namespace Database;

class Field extends Base
{

    public function __construct()
    {
        $this->setModel(MODEL_FIELD);
    }

    public function getIndex($page, $limit, $extension = [])
    {
        $sql = $this->getSql();
        $sql->field('*');
        $sql->table($this->getTable());
        $sql->setPage($page);
        $sql->setPageSize($limit);
        $sql->setLimit();
        $sql->setOrder('dry_sort', 'asc');
        if(isset($extension['formType'])){
            $sql->where('dry_form_type', '=', $extension['formType']);
        }
        $list = $this->fetchAll($sql->get());
        $count = $this->fetchColumn($sql->getTotalSql());
        return [
            'list' => $list,
            'count' => $count
        ];
    }

}