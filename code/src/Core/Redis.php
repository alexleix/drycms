<?php
namespace Core;

class Redis
{

    public function get($key = 'default')
    {
        $config = getConfig('redis', $key);
        $conn = new \Redis();
        if($config['timeout']){
            $conn->connect($config['host'], $config['port'], $config['timeout']);
        }
        else{
            $conn->pconnect($config['host'], $config['port']);
            $conn->setOption(\Redis::OPT_READ_TIMEOUT, -1);
        }
        $conn->auth($config['password']);
        $conn->select($config['dbname']);
        return $conn;
    }

}