<?php
if($groupId <= 0){
    return false;
}
if(!$token){
    return false;
}
$serviceUser = $this->get(SERVICE_USER);
$result = $serviceUser->validToken($device, $token);
if(!$result){
    return false;
}
$userId = $result['id'];
/*用户组是不是所属token的用户*/
$user = $serviceUser->one($userId);
$groupIn = explode(',', $user->dry_group);
if(!in_array($groupId, $groupIn)){
    return false;
}
/*如果是超管*/
$group = $this->get(SERVICE_GROUP)->one($groupId);
if($group->dry_alias == 'GROUP_SUPER_ADMIN'){
    return true;
}