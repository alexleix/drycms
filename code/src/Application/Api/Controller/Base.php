<?php
namespace Application\Api\Controller;

class Base
{

    use \CommonTrait\Base;

    use \CommonTrait\Controller;

    public $request = null;

    public $response = null;

    public $manualSetContentType = false;

    public function __construct($request = null, $response = null)
    {
        if(!$request || !$response){
            return false;
        }
        $this->request = $request;
        $this->response = $response;
        $this->cors();
        if(!$this->manualSetContentType){
            $this->json();
        }
        /*检查权限 start*/
        $bool = $this->get(SERVICE_PERMISSION)->check($request, $this->getRequestData(), $this->getGroup(), $this->getToken());
        if(!$bool){
            print_r($request->server['request_uri']);
            $this->sendFail('SIGN_ERROR');
            return false;
        }
        /*检查权限 end*/
    }

    /*
        控制器也可以当成服务使用，需要伪装请求对象和响应对象
        $data 请求的数据
    */
    public function mask($data = [])
    {
        $request = object();
        $request->get = $data;
        $request->post = [];
        $this->request = $request;
        $this->response = $this->get(SERVICE_RESPONSEMASK);
        return $this;
    }

}