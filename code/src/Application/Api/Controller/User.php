<?php
namespace Application\Api\Controller;

class User extends Base
{

    private function getService()
    {
        return $this->get(SERVICE_USER);
    }

    public function loginAction()
    {
        $data = $this->getRequestData();
        $service = $this->getService();
        if(notSetOrEmpty($data, 'username')){
            $this->sendFail('PARAMETER_ERROR', 'username参数错误');
            return false;
        }
        if(notSetOrEmpty($data, 'password')){
            $this->sendFail('PARAMETER_ERROR', 'password参数错误');
            return false;
        }
        if(notSetOrEmpty($data, 'validCode')){
            $this->sendFail('PARAMETER_ERROR', 'validCode参数错误');
            return false;
        }
        if(notSetOrEmpty($data, 'validCodeName')){
            $this->sendFail('PARAMETER_ERROR', 'validCodeName参数错误');
            return false;
        }
        if(notSetOrEmpty($data, 'device')){
            $this->sendFail('PARAMETER_ERROR', 'device参数错误');
            return false;
        }
        if(notSetOrEmpty($data, 'csrf')){
            $this->sendFail('PARAMETER_ERROR', 'csrf参数错误');
            return false;
        }
        /*csrf是否被篡改或者过期*/
        if(!csrfDecrypt($data['csrf'])){
            $this->sendFail('CSRF_ERROR');
            return false;
        }
        /*验证码是否正确*/
        $cacheCommon = $this->get(CACHE_COMMON);
        $code = $cacheCommon->getData($data['validCodeName']);
        if(!checkValidCode($code, $data['validCode'])){
            $this->sendFail('VALID_CODE_ERROR');
            return false;
        }
        /*验证账号*/
        $user = $service->loginByPassword($data['username'], $data['password']);
        if(hasFail($user)){
            $this->send($user);
            return false;
        }
        /*返回token*/
        $token = $service->setToken($data['device'], $user->id);
        $this->sendSuccess(['token' => $token]);
    }

    public function logoutAction()
    {
        $data = $this->getRequestData();
        $sessionId = tryGet($data, 'sessionId');
        $token = tryGet($data, 'token');
        $device = tryGet($data, 'device');
        if($sessionId){
            $this->get(CORE_SESSION)->init($sessionId)->del();
        }
        if($token){
            $service = $this->getService();
            $result = $service->validToken($device, $token);
            if($result){
                $service->deleteToken($device, $result['id']);
            }
        }
        $this->sendSuccess();
    }

    public function isLoginAction()
    {
        $data = $this->getRequestData();
        $token = tryGet($data, 'token');
        $device = tryGet($data, 'device');
        if(!$token){
            $this->sendFail('TOKEN_ERROR');
            return false;
        }
        $service = $this->getService();
        $result = $service->validToken($device, $token);
        if($result){
            $this->sendSuccess();
        }
        else{
            $this->sendFail('TOKEN_ERROR');
        }
    }

    public function selectUserAction()
    {
        $data = $this->getRequestData();
        $token = tryGet($data, 'token');
        $userId = (int)tryGet($data, 'userId');
        $device = tryGet($data, 'device');
        if($userId <= 0){
            $this->sendFail('USERNAME_NOT_EXIST', '', __LINE__);
            return false;
        }
        if(!$token){
            $this->sendFail('TOKEN_ERROR', '', __LINE__);
            return false;
        }
        $service = $this->getService();
        $result = $service->validToken($device, $token);
        if(!$result){
            $this->sendFail('TOKEN_ERROR', '', __LINE__);
            return false;
        }
        /*选的用户是不是所属token的用户*/
        $tokenUserId = $result['id'];
        $userIdIn = $this->get(SERVICE_DRY)->getSubUserList($tokenUserId, true);
        if(!in_array($userId, $userIdIn)){
            $this->sendFail('TOKEN_ERROR', '', __LINE__);
            return false;
        }
        /*选择的用户是否被禁用*/
        $user = $service->one($userId);
        if($user->dry_status == 0){
            $this->sendFail('USERNAME_STATUS_ERROR', '', __LINE__);
            return false;
        }
        /*返回token*/
        $token = $service->setToken($device, $userId);
        $this->sendSuccess(['token' => $token]);
    }

    public function selectGroupAction()
    {
        $data = $this->getRequestData();
        $token = tryGet($data, 'token');
        $groupId = (int)tryGet($data, 'groupId');
        $device = tryGet($data, 'device');
        if($groupId <= 0){
            $this->sendFail('GROUP_NOT_EXIST', '', __LINE__);
            return false;
        }
        if(!$token){
            $this->sendFail('TOKEN_ERROR', '', __LINE__);
            return false;
        }
        $service = $this->getService();
        $result = $service->validToken($device, $token);
        if(!$result){
            $this->sendFail('TOKEN_ERROR', '', __LINE__);
            return false;
        }
        $userId = $result['id'];
        /*选的用户组是不是所属token的用户*/
        $user = $service->one($userId);
        $groupIn = explode(',', $user->dry_group);
        if(!in_array($groupId, $groupIn)){
            $this->sendFail('ILLEGAL_ERROR', '', __LINE__);
            return false;
        }
        $this->sendSuccess();
    }

    public function getUserAction()
    {
        $data = $this->getRequestData();
        $device = tryGet($data, 'device');
        $token = tryGet($data, 'token');
        $service = $this->getService();
        $user = $service->getUserFromToken($device, $token);
        if(empty($user)){
            $this->sendFail();
            return false;
        }
        else{
            $user = $service->addUserAccountInformation($user);
            $user = $service->addUserIncomeInformation($user);
            $user = $service->addUserCodeInformation($user);
            $send = [
                'user' => $user
            ];
            $this->sendSuccess($send);
        }
    }

    public function updatePasswordAction()
    {
        $data = $this->getRequestData();
        $device = tryGet($data, 'device');
        $token = tryGet($data, 'token');
        $service = $this->getService();
        $corePassword = $this->get(CORE_PASSWORD);
        $user = $service->getUserFromToken($device, $token);
        if(empty($user)){
            $this->sendFail('TOKEN_ERROR');
            return false;
        }
        $bool = $corePassword->passwordVerify($data['dry_old_password'], $user->dry_password);
        if(!$bool){
            $this->sendFail('OLD_PASSWORD_ERROR');
            return false;
        }
        if(!checkPassword($data['dry_password'])){
            $this->sendFail('PASSWORD_FORMAT_ERROR');
            return false;
        }
        if($data['dry_password'] != $data['dry_sure_password']){
            $this->sendFail('PASSWORDS_DOES_NOT_MATCH');
            return false;
        }
        $service->updatePassword($user->id, $data['dry_password']);
        $this->sendSuccess();
    }

}