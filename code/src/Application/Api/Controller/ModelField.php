<?php
namespace Application\Api\Controller;

class ModelField extends Base
{

    private function getService()
    {
        return $this->get(SERVICE_MODELFIELD);
    }

    public function indexAction()
    {
        $data = $this->getRequestData();
        $extension = [];
        if(isset($data['modelId'])){
            $extension['modelId'] = $data['modelId'];
        }
        $send = $this->getService()->getIndex($data['page'], $data['limit'], $extension);
        /**/
        $list = $send['list'];
        $fieldIdIn = array2stringByKey($list, 'dry_field');
        $fieldList = $this->get(SERVICE_FIELD)->more($fieldIdIn);
        $map = array2mapObject($fieldList, 'id');
        foreach($list as $k => $v){
            $field = $map[$v->dry_field];
            $v->dry_database_field = $field->dry_field;
            $v->dry_form_type = $field->dry_form_type;
            $list[$k] = $v;
        }
        /**/
        $this->sendSuccess($send);
    }

    public function newAction()
    {

    }

    public function editAction()
    {
        $service = $this->getService();
        $data = $this->getRequestData();
        $id = $data['id'];
        unset($data['id']);
        $rows = $service->update($data, $id);
        $send = [
            'rows' => $rows
        ];
        $this->sendSuccess($send);
    }

    public function editPlusAction()
    {
        $service = $this->getService();
        $data = $this->getRequestData();
        $id = (int)$data['id'];
        $field = $data['field'];
        $value = $data['value'];
        $update = [
            $field => $value
        ];
        $service->update($update, $id);
        $this->sendSuccess();
    }

}