<?php
namespace Application\Api\Controller;

class Field extends Base
{

    private function getService()
    {
        return $this->get(SERVICE_FIELD);
    }

    public function indexAction()
    {
        $data = $this->getRequestData();
        $extension = [];
        if(isset($data['formType'])){
            $extension['formType'] = $data['formType'];
        }
        $send = $this->getService()->getIndex($data['page'], $data['limit'], $extension);
        $this->sendSuccess($send);
    }

    public function newAction()
    {
        $service = $this->getService();
        $data = $this->getRequestData();
        $data = addDateTime($data, THE_TIME);
        $id = $service->add($data);
        $send = [
            'id' => $id
        ];
        $this->sendSuccess($send);
    }

    public function editAction()
    {
        $service = $this->getService();
        $data = $this->getRequestData();
        $id = $data['id'];
        unset($data['id']);
        $rows = $service->update($data, $id);
        $send = [
            'rows' => $rows
        ];
        $this->sendSuccess($send);
    }

    public function getListAction()
    {
        $service = $this->getService();
        $list = $service->getList();
        $listNew = [];
        foreach($list as $v){
            $text = $v->dry_name;
            if($v->dry_note != ''){
                $text = $v->dry_name . "({$v->dry_note})";
            }
            $listNew[] = [
                'key' => $v->id,
                'text' => $text,
                'class' => ''
            ];
        }
        $send = [
            'list' => $listNew
        ];
        $this->sendSuccess($send);
    }

}