<?php
namespace Application\Admin\Controller;

class Qr extends Base
{

    public function init()
    {
        $this->setDir('qr');
    }

    public function loginAction()
    {
        $this->setFile($this->getTemplateLogin());
        $data = [];
        $this->setData($data);
        $this->show();
    }

}