<?php
namespace Model;

class Base
{

    public function getConfig()
    {
        return $this->config;
    }

    public function getTable()
    {
        return $this->table;
    }

}