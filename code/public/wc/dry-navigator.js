class dryNavigator extends HTMLElement{
    constructor(){
        super();
        var that = this;
        var url = get("url");
        var ref = get("ref");
        var callback = get("callback");
        var shrink = get("shrink");
        var unique = get("unique");
        var filter = get("filter");
        var vclass = get("class");
        var fold = get("fold");
        var icon = get("icon");
        
        function get(name, theDefault = "")
        {
            if(that.hasAttribute(name)){
                return that.getAttribute(name);
            }
            return theDefault;
        }
        
        function $$(id)
        {
            return document.getElementById(id);
        }
        
        function getUnique()
        {
            var d = new Date();
            var temp = Math.random() + "";
            return "g_" + d.getTime() + "_" + temp.substr(2);
        }
        
        function getRequest(url, successCallback, failCallback)
        {
            function success(response)
            {
                if(response.ok == true && response.status == 200){
                    response.json().then(function(json){
                        successCallback(json);
                    });
                }
            }
            
            function fail(response)
            {
                if(failCallback){
                    failCallback(response);
                }
            }
            
            fetch(url).then(success).catch(fail);
        }
        
        function commonCallback(y)
        {
            y.element.render();
        }
        
        function runCallback(p)
        {
            var f = window[callback];
            if(!f){
                setTimeout(function(){
                    runCallback(p);
                }, 500);
            }
            else{
                f(p);
                that.setAttribute("ok", "1");
            }
        }

        function parse(data)
        {
            var children = [];
            var hasParent = [];
            var topList = [];
            
            function hasChildren(item)
            {
                if(children[item.id] && children[item.id].length){
                    return true;
                }
                return false;
            }
            
            function doChildren(item)
            {
                var ddList = [];
                ddList.push(`<dd>`);
                var a = `<a href='javascript:void(0);' lay-href='@href'>@name</a>`;
                if(hasChildren(item)){
                    a = `<a href='javascript:void(0);'>@name</a>`;
                }
                ddList.push(a.replace("@name", item.dry_title).replace("@href", item.dry_link));
                if(hasChildren(item)){
                    var childrenList = children[item.id];
                    ddList.push(`<dl class='layui-nav-child'>`);
                    for(var j in childrenList){
                        ddList.push(doChildren(childrenList[j]));
                    }
                    ddList.push(`</dl>`);
                }
                ddList.push(`</dd>`);
                return ddList.join("");
            }
            
            for(var i in data){
                var item = data[i];
                hasParent[item.id] = false;
                if(item.dry_parent > 0){
                    hasParent[item.id] = true;
                    if(!children[item.dry_parent]){
                        children[item.dry_parent] = [];
                    }
                    children[item.dry_parent].push(item);
                }
                if(!hasParent[item.id]){
                    topList.push(item);
                }
            }
            
            var liList = [];
            for(var i in topList){
                var item = topList[i];
                var cssClass = "";
                if(fold == "0"){
                    cssClass = "layui-nav-itemed";
                }
                liList.push(`<li class='layui-nav-item layui-unselect ${cssClass}'>`);
                var iconCode = `<i class='layui-icon ${item.dry_icon}'></i>`;
                if(icon == "0"){
                    iconCode = "";
                }
                liList.push(`<a href='javascript:void(0);'>${iconCode}<cite>${item.dry_title}</cite></a>`);
                if(hasChildren(item)){
                    var childrenList = children[item.id];
                    liList.push(`<dl class='layui-nav-child'>`);
                    for(var j in childrenList){
                        liList.push(doChildren(childrenList[j]));
                    }
                    liList.push(`</dl>`);
                }
                liList.push(`</li>`);
            }
            
            return liList.join("");
        }
        
        function finish(json)
        {
            var li = parse(json.data);
            var ul = `<ul class="layui-nav ${vclass}" lay-shrink="${shrink}" id="${unique}" lay-filter="${filter}">${li}</ul>`;
            if(ref == ""){
                ref = getUnique();
                that.insertAdjacentHTML("afterend", `<div id="${ref}"></div>`);
            }
            $$(ref).innerHTML = ul;
            runCallback(commonCallback);
        }
        
        if(callback == ""){
            callback = "dryCallback";
        }
        getRequest(url, finish);
    }
};
window.customElements.define('dry-navigator', dryNavigator);