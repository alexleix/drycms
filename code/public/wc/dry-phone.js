class dryPhone extends HTMLElement{
    constructor(){
        super();
        var that = this;
        var ref = get("ref");
        var callback = get("callback");
        var label = get("label");
        var url = get("url");
        var cardColor = get("cardColor", "#009688");
        var phoneName = get("phoneName", "dry_phone");
        var phonePlaceholder = get("phonePlaceholder", "请输入手机号");
        var codeName = get("codeName", "dry_code");
        var codePlaceholder = get("codePlaceholder", "请输入短信验证码");
        var buttonClass = get("buttonClass", "");
        var regetButtonClass = get("regetButtonClass", "layui-btn-disabled");
        var time = get("time", 60);
        var functionName = get("functionName");
        
        function get(name, theDefault = "")
        {
            if(that.hasAttribute(name)){
                return that.getAttribute(name);
            }
            return theDefault;
        }
        
        function $$(id)
        {
            return document.getElementById(id);
        }
        
        function isPhone(test)
        {
            return /^1[3-8]{1}\d{9}$/.test(test);
        }
        
        function getUnique()
        {
            var d = new Date();
            var temp = Math.random() + "";
            return "g_" + d.getTime() + "_" + temp.substr(2);
        }
        
        function getRequest(url, successCallback, failCallback)
        {
            function success(response)
            {
                if(response.ok == true && response.status == 200){
                    response.json().then(function(json){
                        successCallback(json);
                    });
                }
            }
            
            function fail(response)
            {
                if(typeof(failCallback) == "function"){
                    failCallback(response);
                }
                else{
                    console.log(response);
                }
            }
            
            fetch(url).then(success).catch(fail);
        }
        
        function runCallback(p)
        {
            var f = window[callback];
            if(!f){
                setTimeout(function(){
                    runCallback(p);
                }, 500);
            }
            else{
                f(p);
                that.setAttribute("ok", "1");
            }
        }
        
        function finish()
        {
            var html = [];
            html.push(`<div class="layui-form-item">`);
            /*layui-card start*/
            html.push(`<div class="layui-card">`);
            html.push(`<div class="layui-card-header" style="background-color:${cardColor};border:1px solid ${cardColor};color:white;">${label}</div>`);
            html.push(`<div class="layui-card-body" style="border:1px solid ${cardColor};margin-top:-1px;">`);
            /*layui-card-body start*/
            html.push(`<input type="text" class="layui-input" xautocomplete="off" name="${phoneName}" id="${phoneName}" lay-verify="${phoneName}" placeholder="${phonePlaceholder}">`);
            html.push(`<input type="text" class="layui-input" autocomplete="off" name="${codeName}" id="${codeName}" lay-verify="${codeName}" placeholder="${codePlaceholder}" style="margin-top:10px;">`);
            html.push(`<button type="button" class="layui-btn layui-btn-fluid ${buttonClass} dry-get-button" style="margin-top:10px;">获取短信验证码</button>`);
            html.push(`<button type="button" class="layui-btn layui-btn-fluid ${regetButtonClass} dry-reget-button" style="margin-top:10px;margin-left:0;display:none;"><span>${time}</span>秒后重试</button>`);
            /*layui-card-body end*/
            html.push(`</div>`);
            html.push(`</div>`);
            /*layui-card end*/
            html.push(`</div>`);
            if(ref == ""){
                ref = getUnique();
                that.insertAdjacentHTML("afterend", `<div id="${ref}"></div>`);
            }
            $$(ref).innerHTML = html.join("");
            runCallback(commonCallback);
        }
        
        function commonCallback(y, f)
        {
            function render()
            {
                form.render();
            }
            
            function success()
            {
                layer.msg("短信验证码已发送");
            }
            
            function init()
            {
                var getButton = $("#" + phoneName).parent().find(".dry-get-button");
                var regetButton = $("#" + phoneName).parent().find(".dry-reget-button");
                var countDown = regetButton.find("span");
                var lock = 0;
                getButton.click(function(){
                    /*锁 start*/
                    if(lock){
                        return false;
                    }
                    lock = 1;
                    window.setTimeout(function(){
                        lock = 0;
                    }, 1000);
                    /*锁 end*/
                    var phone = $("#" + phoneName).val();
                    if(!isPhone(phone)){
                        layer.msg('手机号不正确');
                        return false;
                    }
                    /*如果设置了发送验证码的处理函数则优先用自定义函数处理*/
                    if(typeof(f) == "object" && f[functionName] != undefined){
                        f[functionName](phoneName);
                    }
                    else{
                        getRequest(url + phone, success);
                    }
                    getButton.hide();
                    regetButton.show();
                    /*倒计时 start*/
                    var theId = window.setInterval(function(){
                        var i = countDown.html();
                        i--;
                        if(i <= 0){
                            countDown.html(time);
                            window.clearInterval(theId);
                            getButton.show();
                            regetButton.hide();
                        }
                        else{
                            countDown.html(i);
                        }
                    }, 1000);
                    /*倒计时 end*/
                });
            }
            
            var form = y.form;
            var $ = y.jquery;
            var layer = y.layer;
            init();
            render();
        }
        
        if(callback == ""){
            callback = "dryCallback";
        }
        finish();
    }
};
window.customElements.define('dry-phone', dryPhone);