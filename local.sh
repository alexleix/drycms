#!/bin/bash
echo 'please select:'
echo '1.docker images'
echo '2.docker ps -a'
echo '3.docker rm -f @container'
echo '4.docker rmi -f @image'
echo '5.docker build -t @image .'
echo '6.[dev]docker-compose up -d'
echo '7.[pro]docker-compose up -d'
echo '8.docker exec -it @container sh'
echo '9.docker push @image'
image=drycms
container=drycms
read name
case $name in
    '1')
        sudo docker images
        ;;
    '2')
        sudo docker ps -a
        ;;
    '3')
        sudo docker rm -f $container
        ;;
    '4')
        sudo docker rmi -f $image
        ;;
    '5')
        echo 'local|product'
        read theEnv
        sed "s/@env/${theEnv}/g" ./run-from.sh > ./run.sh
        sudo docker build -t $image .
        rm -rf ./run.sh
        ;;
    '6')
        sudo docker-compose --file=docker-compose.yml up -d
        ;;
    '7')
        sudo docker-compose --file=docker-compose.yml up -d
        ;;
    '8')
        sudo docker exec -it $container sh
        ;;
    '9')
        sudo docker push $image
        ;;
    *)
        echo "not find"
        ;;
esac
